### About task

Програма отримує з серверу (використати будь-яку зручну його імітацію) список шаблонів та зберігає його в пам'яті.

На сторінці 1 відобразити таблицю шаблонів що містить ім'я і час останньої зміни шаблону.
Ім'я має бути посиланням на сторінку деталей шаблону.

Сторінка 2 має відмалювати зміст HTML шаблону.
Додати можливість виділення будь-якого елементу відмальованого шаблону що містить клас '.editable' мишою.
У випадку виділення елементу відобразити панель, що дозволить встановити текст та розмір шрифту елементу.
Після кожної зміни шаблону імітувати його збереження до серверу та оновлювати список шаблонів в пам'яті.
Після повернення на сторінку 1 таблиця має містити коректний час останньої зміни

# HtmlTemplatesFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
