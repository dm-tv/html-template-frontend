import {Validators} from '@angular/forms';
import {VALIDATION_MESSAGE} from '../../../core/helpers/constants';

const fieldName: any = {
    text: 'Text',
    fontSize: 'Font size'
};

export const templateEditorSchema: any = {
    text: ['', [
        Validators.required
    ]],
    fontSize: ['', [
        Validators.required,
        Validators.pattern(/^\d+$/)
    ]]
};

export const templateEditorMessages: any = {
    text: {
        required: VALIDATION_MESSAGE.common.required(fieldName.text)
    },
    fontSize: {
        required: VALIDATION_MESSAGE.common.required(fieldName.fontSize),
        pattern: VALIDATION_MESSAGE.fontSize.pattern
    }

};
