import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BehaviorSubject} from 'rxjs';

import {templateEditorMessages, templateEditorSchema} from './html-template-editor.config';
import {TemplateEditorOutput} from '../../../core/interfaces/forms/TemplateEditorOutput';

@Component({
    selector: 'app-html-template-editor',
    templateUrl: './html-template-editor.component.html',
    styleUrls: ['./html-template-editor.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HtmlTemplateEditorComponent implements OnInit, OnChanges {

    templateEditorForm: FormGroup;
    formErrors: BehaviorSubject<any>;
    errorMessages: any;

    @Input('element') element: Element;
    @Output() change: EventEmitter<TemplateEditorOutput> = new EventEmitter<TemplateEditorOutput>();

    constructor(private fb: FormBuilder) {
        this.templateEditorForm = this.fb.group(templateEditorSchema);
        this.formErrors = new BehaviorSubject({});
        this.errorMessages = templateEditorMessages;
    }

    static getFontSize(el: Element): number {
        const elementStyle = window.getComputedStyle(el, null).getPropertyValue('font-size');
        return parseFloat(elementStyle);
    }

    ngOnInit(): void {
        this.setDefaultValues();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.element) {
            this.element = changes['element'].currentValue;
        }
        this.setDefaultValues();
    }

    setDefaultValues() {
        this.templateEditorForm.get('text').setValue(this.element.innerHTML);
        this.templateEditorForm.get('fontSize').setValue(HtmlTemplateEditorComponent.getFontSize(this.element));
    }

    checkControlErrors(controlName: string): void {
        this.formErrors.getValue()[controlName] = [];
        if (this.templateEditorForm.controls[controlName].errors) {
            for (const prop of Object.keys(this.templateEditorForm.controls[controlName].errors)) {
                this.formErrors.getValue()[controlName].push(this.errorMessages[controlName][prop]);
            }
        }
    }

    checkFormErrors(): void {
        if (this.templateEditorForm.controls) {
            for (const control of Object.keys(this.templateEditorForm.controls)) {
                this.checkControlErrors(control);
            }
        }
    }

    saveTemplate(): void {
        this.checkFormErrors();
        if (this.templateEditorForm.valid) {
           this.change.emit({
               text: this.templateEditorForm.get('text').value,
               fontSize: this.templateEditorForm.get('fontSize').value
           });
        }
    }
}
