import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';

import {HtmlTemplatesService} from '../../../core/services/html-templates.service';
import {HtmlTemplate} from '../../../core/interfaces/models/HtmlTemplate';

@Component({
    selector: 'app-html-template-list',
    templateUrl: './html-template-list.component.html',
    styleUrls: ['./html-template-list.component.css']
})
export class HtmlTemplateListComponent implements OnInit {
    templates: HtmlTemplate[];
    displayedColumns: string[] = ['id', 'name', 'modified'];
    dataSource: MatTableDataSource<HtmlTemplate>;

    constructor(private htmlTemplatesService: HtmlTemplatesService) {
    }

    ngOnInit() {
        this.htmlTemplatesService.getList()
            .subscribe((templates: HtmlTemplate[]) => {
                this.templates = templates;
                this.dataSource = new MatTableDataSource(this.templates);
            });
    }


}
