import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule, MatInputModule} from '@angular/material';

import {HtmlTemplateListComponent} from './html-template-list/html-template-list.component';
import {HtmlTemplateDetailedComponent} from './html-template-detailed/html-template-detailed.component';
import {HTML_TEMPLATE_ROUTES} from './html-template.router';
import { HtmlTemplateEditorComponent } from './html-template-editor/html-template-editor.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(HTML_TEMPLATE_ROUTES),
        MatTableModule,
        MatButtonModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [HtmlTemplateListComponent, HtmlTemplateDetailedComponent, HtmlTemplateEditorComponent]
})
export class HtmlTemplateModule {
}
