import {Routes} from '@angular/router';

import {HtmlTemplateListComponent} from './html-template-list/html-template-list.component';
import {HtmlTemplateDetailedComponent} from './html-template-detailed/html-template-detailed.component';

export const HTML_TEMPLATE_ROUTES: Routes = [
    {
        path: '',
        children: [
            {
                path: 'list',
                component: HtmlTemplateListComponent
            },
            {
                path: ':id',
                component: HtmlTemplateDetailedComponent
            },
            {
                path: '**',
                pathMatch: 'full',
                redirectTo: 'list'
            }
        ]
    },

];
