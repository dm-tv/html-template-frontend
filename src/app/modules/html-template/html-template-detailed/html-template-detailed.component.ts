import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import {style, animate, transition, trigger} from '@angular/animations';
import {switchMap} from 'rxjs/operators';

import {HtmlTemplatesService} from '../../../core/services/html-templates.service';
import {TemplateEditorOutput} from '../../../core/interfaces/forms/TemplateEditorOutput';

@Component({
    animations: [
        trigger('fadeInOut', [
            transition(':enter', [
                style({opacity: 0}),
                animate(500, style({opacity: 1}))
            ]),
            transition(':leave', [
                animate(500, style({opacity: 0}))
            ])
        ])
    ],
    selector: 'app-html-template-detailed',
    templateUrl: './html-template-detailed.component.html',
    styleUrls: ['./html-template-detailed.component.css']
})
export class HtmlTemplateDetailedComponent implements OnInit {
    activeTemplate: SafeHtml;
    activeTemplateId: number;
    activeSelection: Element = null;
    isTemplateEditorOpened = false;

    @ViewChild('template') template: ElementRef;


    constructor(private activatedRoute: ActivatedRoute,
                private htmlTemplatesService: HtmlTemplatesService,
                private domSanitizer: DomSanitizer) {
    }

    ngOnInit(): void {
        this.activatedRoute.params
            .pipe(switchMap((params) => {
                    this.activeTemplateId = +params.id;
                    return this.htmlTemplatesService.getOne(this.activeTemplateId);
                }
            ))
            .subscribe((template) => {
                this.activeTemplate = this.domSanitizer.bypassSecurityTrustHtml(template.template);
            });
    }

    onSelectText(event): void {
        this.showTemplateEditor(event.target);
    }

    showTemplateEditor(el: Element): void {
        const selection: Selection = window.getSelection();
        if (selection.toString().length === 0) {
            return;
        }

        if (!el.classList.contains('editable')) {
            return;
        }

        this.activeSelection = el;
        this.isTemplateEditorOpened = true;
    }

    renderTemplate(formValues: TemplateEditorOutput): void {
        if (!formValues) {
            this.closeTemplateEditor();
            return;
        }

        this.activeSelection.innerHTML = formValues.text;
        this.activeSelection['style'].fontSize = formValues.fontSize + 'px';
        this.htmlTemplatesService.updateOne(this.activeTemplateId, this.template.nativeElement.innerHTML)
            .subscribe(() => {
                // Do nothing
            });
    }

    closeTemplateEditor(): void {
        this.isTemplateEditorOpened = false;
    }
}
