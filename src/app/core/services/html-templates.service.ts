import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {tap, map} from 'rxjs/operators';

import {ResponseList} from '../interfaces/ResponseList';
import {HtmlTemplate} from '../interfaces/models/HtmlTemplate';
import {API} from '../helpers/constants';
import {AppStorage} from '../helpers/AppStorage';

const APP_TEMPLATES = 'templates';

@Injectable()
export class HtmlTemplatesService {
    private templatesStorage = new AppStorage<HtmlTemplate[]>(localStorage, APP_TEMPLATES);

    constructor(private httpClient: HttpClient) {
    }

    getList(): Observable<HtmlTemplate[]> {
        const ts = this.templatesStorage.getItem();
        if (ts) {
            return new BehaviorSubject<HtmlTemplate[]>(ts).asObservable();
        } else {
            return this.httpClient
                .get<ResponseList<HtmlTemplate>>(API.HTML_TEMPLATES)
                .pipe(map(response => response.rows))
                .pipe(tap(templates => this.templatesStorage.setItem(templates)));
        }

    }

    getOne(templateId: number): Observable<HtmlTemplate> {
        const template: HtmlTemplate = this.templatesStorage.getItem().find((t) => t.id === templateId);
        return new BehaviorSubject<HtmlTemplate>(template).asObservable();
    }

    updateOne(templateId: number, updTemplate: string): Observable<HtmlTemplate> {
        const templates: HtmlTemplate[] = this.templatesStorage.getItem();
        const updTemplateIndex: number = templates.findIndex((t) => t.id === templateId);
        templates[updTemplateIndex].modified = Date.now();
        templates[updTemplateIndex].template = updTemplate;
        this.templatesStorage.setItem(templates);

        return new BehaviorSubject<HtmlTemplate>(templates[updTemplateIndex]).asObservable();
    }
}
