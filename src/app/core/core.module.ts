import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {URLComposerInterceptor} from './interceptors/URLComposer.interceptor';
import {HtmlTemplatesService} from './services/html-templates.service';


@NgModule({
    imports: [
        CommonModule,
        HttpClientModule
    ],
    declarations: [],
    providers: [
        HtmlTemplatesService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: URLComposerInterceptor,
            multi: true
        }
    ]
})
export class CoreModule {
}
