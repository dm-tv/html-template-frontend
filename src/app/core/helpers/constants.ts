export const API = {
    HTML_TEMPLATES: `/html-templates`
};

export const VALIDATION_MESSAGE: any = {
    common: {
        required(fieldName): string {
            return `${fieldName} field is empty`;
        }
    },
    fontSize: {
        pattern: 'Field must consists only digits'
    }
};
