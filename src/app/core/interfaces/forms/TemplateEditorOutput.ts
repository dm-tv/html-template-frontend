export interface TemplateEditorOutput {
    text: string;
    fontSize: number;
}
