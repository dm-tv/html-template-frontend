import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

import {API_URL} from '../../../environments/environment';

@Injectable()
export class URLComposerInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let userReq;
        userReq = req.clone({
            url: [API_URL, req.url].join('')
        });

        // Pass on the cloned request instead of the original request.
        return next.handle(userReq);
    }
}
