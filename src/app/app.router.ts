import {Routes} from '@angular/router';

export const ROUTES: Routes = [
    {
        path: '',
        children: [
            {
                path: 'html-template',
                loadChildren: 'src/app/modules/html-template/html-template.module#HtmlTemplateModule'
            },
            {
                path: '**',
                redirectTo: 'html-template',
                pathMatch: 'full'
            }
        ]
    }
];
